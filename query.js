function d3Jquery(){
    json = post_cypherquery();
    console.log(json);
  force
      .nodes(json.nodes)
      .links(json.links)
      .start();

  var link = svg.selectAll(".link")
      .data(json.links)
    .enter().append("line")
      .attr("class", "link")
    .style("stroke-width", function(d) { return Math.sqrt(d.weight); });

  var node = svg.selectAll(".node")
      .data(json.nodes)
    .enter().append("g")
      .attr("class", "node")
      .call(force.drag);

  node.append("circle")
      .attr("r","15");

  node.append("text")
      .attr("dx", 12)
      .attr("dy", ".65em")
      .text(function(d) { return d.name });

  force.on("tick", function() {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
  });
};

function post_cypherquery() {
    $('#messageArea').html('<h3>...</h3>');
    //Conexion a NEO4J
 
    usr = 'neo4j'; // the user of neo4j database
    psw = '1234'; // the paswword of neo4j database
    usrpsw = usr + ':' + psw;
    authorisation = 'Basic ' + btoa(usrpsw);
    var graph;
    $.ajaxSetup({
        headers: {
        'Authorization': authorisation
        }
    });
    var graph;
    var resultado;
    // PETICION AJAX
    $.ajax({
        // Nos conectamos al 
        url: "http://localhost:7474/db/data/transaction/commit",
        type: 'POST',
        async: false,
        // Realizamos la consulta en:
        // 1. [{"statement" : $query ]}
        // 2  "resultDataContents: ["row", "graph"] 
        // graph = devuelve la estructura gráfica de los nodos y las relaciones devueltas por la consulta.
        // Referencias : " https://neo4j.com/docs/developer-manual/current/http-api/ "
        data: JSON.stringify({ "statements": [{ "statement": $('#cypher-in').val(), "resultDataContents":["graph","row"] }] }),
        contentType: 'application/json',
        accept: 'application/json; charset=UTF-8',                
    }).done(function(data) {
        //console.log(data)
       resultado = data;
        // Las lineas de codigo 31 - 51 sirve para convertir de resultados de la consulta de Neo4j a D3 JSON
        // Referencias: " https://neo4j.com/developer/guide-data-visualization/ "
        // Leer seccion: " Converting Neo4j Query Results to D3 JSON "
        function idIndex(a,id) {
            for (var i=0;i<a.length;i++) {
                if (a[i].id == id) return i;}
            return null;
        }
        var nodes=[], links=[];
        data.results[0].data.forEach(function (row) {
            //console.log(row)
            row.graph.nodes.forEach(function (n) {
                //console.log(n)
                if (idIndex(nodes,n.id) == null){
                    nodes.push({id:n.id,label:n.labels[0],title:n.properties.name,born:n.properties.born});
                }
            });
            links = links.concat( row.graph.relationships.map(function(r) {
                // the neo4j documents has an error : replace start with source and end with target
                return {source:idIndex(nodes,r.startNode),target:idIndex(nodes,r.endNode),type:r.type};
            }));
        });
        graph = {nodes:nodes, links:links};  
         
    }).fail(function (jqXHR, textStatus, errorThrown) {
        $('#messageArea').html('<h3>' + textStatus + ' : ' + errorThrown + '</h3>')
    });
    console.log(graph);
    return graph;
};
  
var width = 960,
    height = 500

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

var force = d3.layout.force()
    .gravity(.05)
    .distance(100)
    .charge(-100)
    .size([width, height]);
